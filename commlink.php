<?php

require_once 'commlink.civix.php';
// phpcs:disable
use CRM_Commlink_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function commlink_civicrm_config(&$config) {
  _commlink_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function commlink_civicrm_xmlMenu(&$files) {
  _commlink_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function commlink_civicrm_install() {
  _commlink_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function commlink_civicrm_postInstall() {
  _commlink_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function commlink_civicrm_uninstall() {
  _commlink_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function commlink_civicrm_enable() {
  _commlink_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function commlink_civicrm_disable() {
  _commlink_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function commlink_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _commlink_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function commlink_civicrm_managed(&$entities) {
  _commlink_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function commlink_civicrm_caseTypes(&$caseTypes) {
  _commlink_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function commlink_civicrm_angularModules(&$angularModules) {
  _commlink_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function commlink_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _commlink_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function commlink_civicrm_entityTypes(&$entityTypes) {
  _commlink_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_themes().
 */
function commlink_civicrm_themes(&$themes) {
  _commlink_civix_civicrm_themes($themes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function commlink_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
//function commlink_civicrm_navigationMenu(&$menu) {
//  _commlink_civix_insert_navigation_menu($menu, 'Mailings', array(
//    'label' => E::ts('New subliminal message'),
//    'name' => 'mailing_subliminal_message',
//    'url' => 'civicrm/mailing/subliminal',
//    'permission' => 'access CiviMail',
//    'operator' => 'OR',
//    'separator' => 0,
//  ));
//  _commlink_civix_navigationMenu($menu);
//}

/**
 * Implements hook_civicrm_pageRun().
 * 
 * Turn email or phone fields into links on Contact Summary pages.
 * 
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun
 */
function commlink_civicrm_pageRun(&$page) {
  $supportedPages = [
    'CRM_Contact_Page_View_Summary',
  ];
  $pageName = get_class($page);

  if (in_array($pageName, $supportedPages)) {
    $jsVars = [
      'phoneExt' => E::ts('ext.'),
    ];
    Civi::resources()
      ->addVars('commlink', $jsVars)
      ->addScriptFile('de.digitalcourage.commlink', 'js/commlink.js')
      ->addScriptFile('de.digitalcourage.commlink', 'js/commlink_contactsummary.js');
    $template = CRM_Core_Smarty::singleton();
  }
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * Turn email or phone fields into links on Search Result forms.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function commlink_civicrm_buildForm($formName, &$form) {
  $supportedForms = [
    'CRM_Contact_Form_Search_Basic',
    'CRM_Contact_Form_Search_Advanced',
  ];

  if (in_array($formName, $supportedForms)) {
    $jsVars = [
      'isProfileResults' => FALSE,
      'emailHeaderLabels' => [],
      'phoneHeaderLabels' => [],
      'phoneExt' => E::ts('ext.'),
    ];
    // Email Profile field handling.
    $ufGroupId = CRM_Utils_Array::value('uf_group_id', $form->_formValues, CRM_Utils_Array::value('uf_group_id', $form->_submitValues));
    if ($ufGroupId) {
      $jsVars['isProfileResults'] = TRUE;
      $emailUFFields = \Civi\Api4\UFField::get()
        ->addWhere('field_name', '=', 'email')
        ->addWhere('uf_group_id', '=', $ufGroupId)
        ->addWhere('is_active', '=', 1)
        ->execute();
      if (!empty($emailUFFields)) {
        foreach ($emailUFFields as $emailUFField) {
          $jsVars['emailHeaderLabels'][] = $emailUFField['label'];
        }
      }
      $phoneUFFields = \Civi\Api4\UFField::get()
        ->addWhere('field_name', '=', 'phone')
        ->addWhere('uf_group_id', '=', $ufGroupId)
        ->addWhere('is_active', '=', 1)
        ->execute();
      if (!empty($phoneUFFields)) {
        foreach ($phoneUFFields as $phoneUFField) {
          $jsVars['phoneHeaderLabels'][] = $phoneUFField['label'];
        }
      }
    } else {
      $jsVars['emailHeaderLabels'][] = E::ts('Email');
      $jsVars['phoneHeaderLabels'][] = E::ts('Phone');
    }

    Civi::resources()
      ->addVars('commlink', $jsVars)
      ->addScriptFile('de.digitalcourage.commlink', 'js/commlink.js')
      ->addScriptFile('de.digitalcourage.commlink', 'js/commlink_searchresults.js');
  }
}
