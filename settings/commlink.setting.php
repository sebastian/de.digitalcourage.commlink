<?php
use CRM_Commlink_ExtensionUtil as E;

return [
    'commlink_mailto_disabled' => [
        'name' => 'commlink_mailto_disabled',
        'type' => 'Boolean',
        'html_type' => 'checkbox',
        'description' => E::ts('<b>Disable</b> insertion of <b>mailto: or activity</b> links in email fields?'),
        'default' => FALSE,
        'settings_pages' => ['commlink' => ['weight' => 10]],
        'title' => E::ts('Disable mailto'),
    ],
    'commlink_mailto_activity' => [
        'name' => 'commlink_mailto_activity',
        'type' => 'Boolean',
        'html_type' => 'checkbox',
        'description' => E::ts('Instead of inserting mailto: links, should CiviCRM email activity links be generated? <br/>This option will not be effective if mail links are disabled via the setting above.'),
        'default' => FALSE,
        'settings_pages' => ['commlink' => ['weight' => 20]],
        'title' => E::ts('Mail activity'),
    ],
    'commlink_tel_disabled' => [
        'name' => 'commlink_tel_disabled',
        'type' => 'Boolean',
        'html_type' => 'checkbox',
        'description' => E::ts('<b>Disable</b> insertion of <b>tel:</b> links in phone number fields?'),
        'default' => FALSE,
        'settings_pages' => ['commlink' => ['weight' => 30]],
        'title' => E::ts('Disable tel'),
    ],
    'commlink_tel_substitution' => [
        'name' => 'commlink_tel_substitution',
        'type' => 'String',
        'html_type' => 'textarea',
        'html_attributes' => [ 'cols' => 32, 'rows' => 4, ],
        'description' => E::ts('<b>Prefix substitution</b> for phone numbers (after removal of non-numbers except for a leading plus)<br/>This is the last step of <b>phone number normalisation</b>.<br/><br/>The <b>steps before</b> the substitution defined in this setting are: Starting with the text content of a phone number field,<br/>&#x2022; the internationalised phrase for &ldquo;ext.&ldquo; is removed<br/>&#x2022; spaces, slashes, hyphens, dashes and dots are removed<br/>&#x2022; a single digit enclosed in parentheses and any other parentheses are removed<br/>&#x2022; the text is reduced to an initial sequence of an optional plus followed by one or more digits.<br/><br/>If this process succeeds, the resulting string can be subjected to <b>one substitution</b> from those defined in the field above. The field is split into lines each representing a possible substitution. A substitution consists of a regular expression for an initial segment of the string (i.e. the regular expression is anchored with a leading <tt>^</tt>), some whitespace, and a (possibly empty) replacement. (The regular expression cannot contain whitespace characters.) The first substitution for which the regular expression matches the string is chosen, and the replacement is applied. In the replacement, <b>$&</b> refers to the entire matched string and <b>$1</b>, <b>$2</b>, &hellip; can be used to refer to subgroups in parentheses.<br/><br/>If a non-empty string remains after all these steps, it is used as the phone number in a tel: link.<br/><br><b>Substitution examples:</b><br/><tt>00 +</tt> &nbsp; &ndash; changes a leading double zero to a plus character to convert international numbers expressed as they can be dialled in most countries to their canonical form<br/><tt>0 +49</tt> &nbsp; &ndash; changes a leading zero to the local country code (Germany for example) under the assumption that it follows the previous substitution (so that no international numbers starting with 00 are affected by this rule)<br/><tt>[1-9] +49521$&</tt> &nbsp; &ndash; inserts country and area codes for a local number starting with a non-zero digit (the city in this example is Bielefeld, Germany)'),
        'default' => '',
        'settings_pages' => ['commlink' => ['weight' => 40]],
        'title' => E::ts('tel substitution'),
    ],
];
