# commlink

![Screenshot](/images/screenshot.png)

The Comm Link extension automatically converts `email` and `phone` fields to display as clickable `mailto:` and `tel:` links, helping save your teams time by adding click-to-email or click-to-dial functionality.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.3+
* CiviCRM v5.34+

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl commlink@https://lab.civicrm.org/extensions/commlink/-/archive/1.1/commlink-1.1.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://gitlab.digitalcourage.de/sebastian/de.digitalcourage.commlink.git
cv en commlink
```

## Getting Started

The extension should just work out-of-the-box. You'll find some configuration options at `civicrm/admin/settings/commlink` where you can control some options.

## Known Issues

None, yet!
