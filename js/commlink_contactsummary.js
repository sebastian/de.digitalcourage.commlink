/**
 * JavaScript code to replace email addresses with mailto: links in contact summary,
 * for CommLink extension.
 */
(function ($, _, ts) {

  commlink.replaceMailLinks = function(jqRoot) {
    var contactId;
    jqRoot.find('span.crm-contact-contact_id').each(function() {
      contactId = $(this).text();
    })
    jqRoot.find('.crm-contact_email a.crm-popup').each(async function() {
      var emailAddress = $(this).text().trim();
      var emailLink = await commlink.createEmailLink(emailAddress, null, contactId);
      if (emailLink) {
        $(this).replaceWith(emailLink);
      }
    });
  };

  commlink.replacePhoneLinks = function(jqRoot) {
    jqRoot.find('.crm-contact_phone').each(function() {
      $(this).contents().filter(function() {
        return this.nodeType === Node.TEXT_NODE;
      }).each(async function() {
        var phoneLink = await commlink.createPhoneLink($(this).text());
        if (phoneLink) {
          $(this).replaceWith(phoneLink);
        }
      });
    });
  };

  commlink.replaceLinks($('#contact-summary'));

}(CRM.$, CRM._, CRM.ts('commlink')));
