(function ($, _, ts) {

  var quotemeta = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }
  var phoneExtRE = new RegExp('([0-9])\\s*' + quotemeta(CRM.vars.commlink.phoneExt) + '\\s*-?([0-9])');

  window.firstText = function(element) {
    var result;
    $(element).find("*").addBack().contents().each(function() {
      if (this.nodeType === Node.TEXT_NODE && (result = this.textContent.trim())) {
        return false;
      }
    });
    return result;
  }

  window.commlink = {

    loadConfig: async function() {
      return CRM.api4('Setting', 'get', {
        select: [
          "commlink_mailto_disabled",
          "commlink_mailto_activity",
          "commlink_tel_disabled",
          "commlink_tel_substitution"
        ]
      }).then(function(settings) {
        if (Array.isArray(settings)) {
          let settingsObj = {};
          settings.forEach((settingsElement) => {
            settingsObj[settingsElement.name] = settingsElement.value;
          });
          settings = settingsObj;
        }
        if (settings.commlink_tel_substitution) {
          settings.substitutions = settings.commlink_tel_substitution
            .split(/\r?\n/)
            .map(function (s) {
              var match = s
                .replace(/[#].*/, "")
                .match(/^(\S+)\s+(.*)/);
              if (match) {
                return [ new RegExp("^" + match[1]), match[2].replace(/\s+$/, "") ];
              } else {
                return null;
              }
            });
        }
        window.commlink.config = settings;
      }, function(failure) {
        console.error(`Failed to retrieve settings from API, error was: ${failure}`)
      })
    },

    replaceLinks: function(jqRoot) {
      $(document).ready(function() {
        commlink.loadConfig().then(function() {
          if (!commlink.config.commlink_mailto_disabled) {
            if (typeof commlink.replaceMailLinks == "function") {
              commlink.replaceMailLinks(jqRoot);
              jqRoot.on('crmLoad', function(event, data) {
                commlink.replaceMailLinks(jqRoot);
              });
            }
          }
          if (!commlink.config.commlink_tel_disabled) {
            if (typeof commlink.replacePhoneLinks == "function") {
              commlink.replacePhoneLinks(jqRoot);
              jqRoot.on('crmLoad', function(event, data) {
                commlink.replacePhoneLinks(jqRoot);
              });
            }
          }
        });
      });
    },

    createEmailLink: async function(emailAddress, linkText, contactId) {
      if (!commlink.config) {
        await commlink.loadConfig();
      }
      var emailLink = null;
      if (emailAddress) {
        var emailLinkAttributes = {
          "class": 'commlink_mail',
          "html": linkText || emailAddress
        };
        if (commlink.config.commlink_mailto_activity && contactId) {
          var emailEntity = await CRM.api4('Email', 'get', {
            where: [
              ["email", "=", emailAddress],
              ["contact_id", "=", contactId]
            ],
            limit: 1
          });
          var emailId = emailEntity["0"]["id"];
          var activityLinkTarget = CRM.url(
            'civicrm/activity/email/add',
            {
              "action": 'add',
              "reset": 1,
              "email_id": emailId
            }
          );
          emailLinkAttributes["title"] = ts('open in CiviCRM');;
          emailLinkAttributes["href"] = activityLinkTarget;
        } else {
          emailLinkAttributes["title"] = ts('Email %emailaddr', { emailaddr: emailAddress });
          emailLinkAttributes["href"] = `mailto:${emailAddress}`;
        }
        emailLink = $('<a/>', emailLinkAttributes);
      }
      return emailLink;
    },

    createPhoneLink: async function(text) {
      if (!commlink.config) {
        await commlink.loadConfig();
      }
      text = text.trim();
      var phoneLink = null;
      if (text) {
        var phoneNumber = text
          .replace(phoneExtRE, "$1-$2")
          .replace(/[\s/.–-]/g, "")
          .replace(/(^[+][0-9]+)[(][0-9][)]([0-9])/, "$1$2")
          .replace(/[()]/g, "")
          .match(/^[+]?[0-9]+/)[0];
        if (phoneNumber) {
          for (var substitution of commlink.config.substitutions) {
            if (substitution && phoneNumber.match(substitution[0])) {
              phoneNumber = phoneNumber.replace(substitution[0], substitution[1]);
              break;
            }
          }
          if (phoneNumber) {
            phoneLink = $('<a/>', {
              class: 'commlink_phone',
              href: `tel:${phoneNumber}`,
              title: ts('Dial %phoneNumber', { phoneNumber: phoneNumber }),
              html: text
            });
          }
        }
      }
      return phoneLink;
    }

  };

}(CRM.$, CRM._, CRM.ts('commlink')));
