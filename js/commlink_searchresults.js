/**
 * JavaScript code to replace email addresses with mailto: links in contact summary,
 * for CommLink extension.
 */
(function ($, _, ts) {

  commlink.replaceMailLinks = function(jqRoot) {
    var emailColumnPositions = [];
    if (CRM.vars.commlink.emailHeaderLabels.length > 0) {
      for (var i in CRM.vars.commlink.emailHeaderLabels) {
        jqRoot.find('thead th').each(function(index) {
          if (firstText(this) == CRM.vars.commlink.emailHeaderLabels[i]) {
            emailColumnPositions.push((index + 1));
          }
        });
      }
    }
    if (emailColumnPositions.length > 0) {
      if (CRM.vars.commlink.isProfileResults) {
        for (var i in emailColumnPositions) {
          jqRoot.find('tbody tr td:nth-child('+ emailColumnPositions[i] +')').each(async function() {
            var emailElement = $(this);
            var emailAddress = emailElement.text().trim();
            var emailLink = await commlink.createEmailLink(emailAddress);
            if (emailLink) {
              emailElement.html(emailLink);
            }
          });
        }
      } else {
        for (var i in emailColumnPositions) {
          jqRoot.find('tbody tr td:nth-child('+ emailColumnPositions[i] +') > span').each(async function() {
            var emailElement = $(this);
            var doNotEmail = emailElement.find('span.do-not-email').length;
            if (!doNotEmail) {
              var emailAddress = emailElement.prop('title').trim();
              var emailTruncated = emailElement.text().trim();
              var viewLink = emailElement.closest('tr').find('a.view-contact');
              var contactIdMatch = viewLink.attr('href').match(/[?&]cid=([0-9]+)(?:&|$)/);
              var contactId = contactIdMatch && contactIdMatch[1];
              var emailLink = await commlink.createEmailLink(emailAddress, emailTruncated, contactId);
              if (emailLink) {
                emailElement.html(emailLink);
              }
            }
          });
        }
      }
    }
  };

  commlink.replacePhoneLinks = function(jqRoot) {
    var phoneColumnPositions = [];
    if (CRM.vars.commlink.phoneHeaderLabels.length > 0) {
      for (var i in CRM.vars.commlink.phoneHeaderLabels) {
        jqRoot.find('thead th').each(function(index) {
          var label = firstText(this);
          if (label && label == CRM.vars.commlink.phoneHeaderLabels[i]) {
            phoneColumnPositions.push((index + 1));
            return false;
          }
        });
      }
    }
    if (phoneColumnPositions.length > 0) {
      if (CRM.vars.commlink.isProfileResults) {
        for (var i in phoneColumnPositions) {
          jqRoot.find('tbody tr td:nth-child('+ phoneColumnPositions[i] +')').each(async function() {
            var phoneElement = $(this);
            var phoneLink = await commlink.createPhoneLink(phoneElement.text());
            if (phoneLink) {
              phoneElement.html(phoneLink);
            }
          });
        }
      } else {
        for (var i in phoneColumnPositions) {
          jqRoot.find('tbody tr td:nth-child('+ phoneColumnPositions[i] +')').each(async function() {
            var phoneElement = $(this);
            var doNotPhone = phoneElement.find('span.do-not-phone').length;
            var doNotSMS = phoneElement.find('span.do-not-sms').length;
            if (!doNotPhone) {
            var phoneLink = await commlink.createPhoneLink(phoneElement.text());
              if (phoneLink) {
                phoneElement.html(phoneLink);
              }
            }
          });
        }
      }
    }
  };

  commlink.replaceLinks($('#crm-main-content-wrapper'));

}(CRM.$, CRM._, CRM.ts('commlink')));
